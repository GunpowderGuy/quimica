from cairosvg import svg2png

noMetales = ["C", "N", "P", "O", "S", "Se", "At", "F", "Cl", "Br", "I", "Ts"]
metaloides = ["Si", "Ge", "B", "As", "Sb", "Te", "Po"]
gasNoble = ["Ne", "Ar", "Kr", "He", "Xe", "Rn", "Og"]
# primero hidrogeno , luego metaloides , luego noMetales , luego gasNoble , luego metal
colores = ["#D1281C", "#18B662", "#C148E6", "#4AD8CA", "#ECBB36"]


def setArchivos(lista, original):
    for i in lista:
        texto = original.replace("Simbolo0a", i[0]).replace("A", i[1]).replace(
        "Z", i[2]).replace("s2p2", i[3]).replace("#ffff00", i[4]).replace("Nombre",i[5])
        texto = texto.replace(i[1]+i[0][-1],i[0]).replace(i[2]+i[0][-1],i[0])
        svg2png(bytestring = texto,write_to = i[0] + ".png")


def elementos():
    Natomicos = ['1', '3', '11', '19', '37', '55', '87', '4', '12', '20', '38'
    , '56', '88', '21', '39', '22', '40', '72', '104', '23', '41', '73'
    , '105', '24', '42', '74', '106', '25', '43', '75', '107', '26', '44'
    , '76', '108', '27', '45', '77', '109', '28', '46', '78', '110',
    '29', '47', '79', '111', '30', '48', '80', '112', '5', '13', '31', '49'
    , '81', '113', '6', '14', '32', '50', '82', '114', '7', '15', '33', '51'
    , '83', '115', '8', '16', '34', '52', '84', '116', '9', '17', '35', '53'
    , '85', '117', '2', '10', '18', '36', '54', '86', '118']

    Simbolos = ['H', 'Li', 'Na', 'K', 'Rb', 'Cs', 'Fr', 'Be', 'Mg', 'Ca', 'Sr', 'Ba', 'Ra', 'Sc'
    , 'Y', 'Ti', 'Zr', 'Hf', 'Rf', 'V', 'Nb', 'Ta', 'Db', 'Cr', 'Mo', 'W', 'Sg', 'Mn', 'Tc', 'Re'
    , 'Bh', 'Fe', 'Ru', 'Os', 'Hs', 'Co', 'Rh', 'Ir', 'Mt', 'Ni', 'Pd', 'Pt', 'Ds','Cu', 'Ag', 'Au'
    , 'Rg', 'Zn', 'Cd', 'Hg', 'Cn', 'B', 'Al', 'Ga', 'In', 'Tl', 'Nh', 'C', 'Si', 'Ge', 'Sn', 'Pb', 'Fl'
    , 'N', 'P', 'As', 'Sb', 'Bi', 'Mc', 'O', 'S', 'Se', 'Te', 'Po', 'Lv', 'F', 'Cl', 'Br', 'I', 'At'
    , 'Ts', 'He', 'Ne', 'Ar', 'Kr', 'Xe', 'Rn', 'Og']

    Valencia = ['1s1', '2s1', '3s1', '4s1', '5s1', '6s1', '7s1', '2s2', '3s2', '4s2', '5s2', '6s2',
    '7s2', '3d14s2', '4d15s2', '3d24s2', '4d25s2', '4f145d26s2', '5f146d27s2', '3d34s2', '4d45s1',
    '4f145d36s2', '5f146d37s2', '3d54s1', '4d55s1', '4f145d46s2', '5f146d47s2', '3d54s2', '4d55s2',
    '4f145d56s2', '5f146d57s2', '3d64s2', '4d75s1', '4f145d66s2', '5f146d67s2', '3d74s2', '4d85s1',
    '4f145d76s2', '5f146d77s2', '3d84s2', '4d10', '4f145d96s1', '5f146d87s2', '3d104s1', '4d105s1',
    '4f145d106s1', '5f146d97s2', '3d104s2', '4d105s2','4f145d106s2', '5f146d107s2', '2s22p1', '3s23p1',
     '3d104s24p1', '4d105s25p1', '4f145d106s26p1', '5f146d107s27p1', '2s22p2', '3s23p2', '3d104s24p2',
      '4d105s25p2', '4f145d106s26p2', '5f146d107s27p2', '2s22p3', '3s23p3', '3d104s24p3', '4d105s25p3',
      '4f145d106s26p3', '5f146d107s27p3', '2s22p4', '3s23p4', '3d104s24p4', '4d105s25p4', '4f145d106s26p4',
      '5f146d107s27p4', '2s22p5', '3s23p5', '3d104s24p5', '4d105s25p5', '4f145d106s26p5', '5f146d107s27p5',
       '1s2', '2s22p6', '3s23p6', '3d104s24p6', '4d105s25p6', '4f145d106s26p6', '5f146d107s27p6']

    Masa = ['1.008', '6.94', '22.990', '39.098', '85.468', '132.91'
    , '223.0', '9.0122', '24.305', '40.078', '87.62', '137.33', '226.0'
    , '44.958', '88.906', '47.867', '91.224', '178.49', '261.0', '50.942'
    , '92.906', '180.95', '262.0', '51.996', '95.95', '183.84', '266.0'
    , '54.938', '98.0', '186.21', '264.0', '55.845', '101.07', '190.23'
    , '277.0', '58.933', '102.91', '192.22', '268.0', '58.693', '106.42'
    , '195.08', '281.0','63.546', '107.87', '196.97', '272.0', '65.38'
    , '112.41', '200.59', '285.0', '10.81', '26.982', '69.723', '114.82'
    , '204.38', '286.0', '12.011', '28.085', '72.630', '118.71', '207.2'
    , '289.0', '14.007', '30.974', '74.922', '121.76', '208.98', '288.0'
    , '15.999', '32.06', '78.971', '127.60', '209.0', '292.0', '18.998'
    , '35.45', '79.904', '126.90', '210.0', '293.0', '20.180', '39.948'
    , '83.798', '131.29', '222.0', '222','294.0']

    Nombres= ['Hidrogeno', 'Litio', 'Sodio', 'Potasio', 'Rubidio', 'Cesio', 'Francio', 'Berilio', 'Magnesio', 'Calcio', 'Estroncio', 'Bario', 'Radio', 'Escandio', 'Itrio', 'Titanio', 'Circonio', 'Hafnio', 'Rutherfordio', 'Vanadio', 'Niobio', 'Tantalo', 'Dubnio', 'Cromo', 'Molibdeno', 'Wolframio', 'Seaborgio', 'Manganeso', 'Tecnecio', 'Renio', 'Bohrio', 'Hierro', 'Rutenio', 'Osmio', 'Hasio', 'Cobalto', 'Rodio', 'Iridio', 'Meitnerio', 'Niquel', 'Paladio', 'Platino', 'Darmstatio', 'Cobre', 'Plata', 'Oro', 'Roentgenio', 'Zinc', 'Cadmio', 'Mercurio', 'Copernicio', 'Boro', 'Aluminio', 'Galio', 'Indio', 'Talio', 'Nihonio', 'Carbono', 'Silicio', 'Germanio', 'Estaño', 'Plomo', 'Flerovio', 'Nitrogeno', 'Fosforo', 'Arsenico', 'Antimonio', 'Bismuto', 'Moscovio', 'Oxigeno', 'Azufre', 'Selenio', 'Telurio', 'Polonio', 'Livermorio', 'Fluor', 'Cloro', 'Bromo', 'Yodo', 'Astato', 'Teneso', 'Helio', 'Neon', 'Argon', 'Kripton', 'Xenon', 'Radon', 'Oganeson']

    Masa = map(lambda x: str(round(float(x))),Masa)

    grupo = map(lambda x: colores[0] if x == "H" else colores[1] if x in metaloides else colores[2]
                     if x in noMetales else colores[3] if x in gasNoble else colores[4], Simbolos)

    return zip(Simbolos, Masa, Natomicos, Valencia, grupo, Nombres)


elementos = elementos()

setArchivos(elementos, open("TemplateCarta.svg", 'r', encoding='utf-8').read())

# https://www.programiz.com/python-programming/file-operation#close
